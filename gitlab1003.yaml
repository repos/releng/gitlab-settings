# Settings for backup Wikimedia production GitLab instance, gitlab2001,
# at https://gitlab-replica-b.wikimedia.org/
#
# Settings reference: https://docs.gitlab.com/ce/api/settings.html
#
# True / false values here should be booleans - necessary type juggling to turn
# these into strings for the API is handled in the script.
#
# Settings not modeled here:
#
#   - Accept Let's Encrypt ToS: Admin Area, Settings, Preferences, Pages, I have
#     read and agree to the Let's Encrypt Terms of Service: checked

# Disable public sign up: Admin Area, Settings, General, Sign-up restrictions,
# Sign-up enabled: unchecked
signup_enabled: false

# Set up logout redirection: Admin Area, Settings, General, Sign-in
# restrictions, After sign-out path: https://<IDP server>/logout, where <IDP
# server> is the base URL of the CAS server, like idp.wmcloud.org or
# idp.wikimedia.org
after_sign_out_path: "https://idp.wikimedia.org/logout"

# Set up private commit emails hostname: Admin Area, Settings, Preferences,
# Email, Custom hostname (for private commit emails):
# users.noreply.<gitlab.domain>, where <gitlab domain> is the base URL of the
# GitLab server, like gitlab.wikimedia.org
commit_email_hostname: "users.noreply.gitlab.wikimedia.org"

# Set up Password authentication: Admin Area, Settings, Sign-in restrictions,
# Password authentication enabled for web interface: unchecked
password_authentication_enabled_for_web: false

# Set up Git over https Password authentication: Admin Area, Settings, Sign-in
# restrictions, Password authentication enabled for Git over HTTP(S): unchecked
password_authentication_enabled_for_git: false

# Disable third party offers: Admin Area, Settings, General, Third party
# offers, Do not display offers from third parties within GitLab: checked
hide_third_party_offers: true

# Default branch name: Admin Area, Settings, Repository, Default initial branch name: set to main
default_branch_name: "main"

# Restrict unauthenticated requests: Admin Area, Settings, Network, User and IP
# Rate Limits, Enable unauthenticated request rate limit: checked
throttle_unauthenticated_enabled: false
throttle_unauthenticated_period_in_seconds: 3600
throttle_unauthenticated_requests_per_period: 3600

# Restrict outbound requests: Admin Area, Settings, Network, Outbound requests,
# Allow requests to the local network from web hooks and services: unchecked
allow_local_requests_from_hooks_and_services: false

# Restrict outbound requests: Admin Area, Settings, Network, Outbound requests,
# Allow requests to the local network from system hooks: unchecked
allow_local_requests_from_system_hooks: false

# Restrict protected paths: Admin Area, Settings, Network, Protected Paths,
# Enable protected paths rate limit: checked
throttle_protected_paths_enabled: true

# Enable Prometheus metrics: Admin Area, Settings, Metrics and profiling,
# Metrics - Prometheus, Enable Prometheus Metrics: checked
prometheus_metrics_enabled: true

# Disable Auto DevOps pipeline: Admin Area, Settings, CI/CD, Continuous
# Integration and Deployment, Default to Auto DevOps pipeline for all projects:
# unchecked
auto_devops_enabled: false

# Set abuse reports email: Admin Area, Settings, Reporting, Abuse reports,
# Abuse reports notification email: set to external abuse reports email
abuse_notification_email: "bbearnes@wikimedia.org"

# Set up RSA SSH keys: Admin Area, Settings, General, Visibility and access
# controls, RSA SSH keys: select must be at least 2048 bits
rsa_key_restriction: 2048

# Forbid DSA SSH keys: Admin Area, Settings, General, Visibility and access
# controls, DSA SSH keys: select are forbidden
dsa_key_restriction: -1

# Disable being OAuth provider: Admin Area, Settings, General, Account and
# limit, Allow users to register any application to use GitLab as an OAuth
# provider: unchecked
user_oauth_applications: false

# Turn off Gravatar for privacy / data exfiltration reasons:
# Admin Area, Settings, Account and Limit, Gravatar enabled unchecked
gravatar_enabled: false

# Set /explore as the default landing page for non-signed-in users - a better
# experience than being immediately sent to a login form:
home_page_url: "https://gitlab-replica-b.wikimedia.org/explore"

# Restrict available visibility levels for new projection creation to public:
restricted_visibility_levels:
  - internal
  - private

# Set default project & group visibility to public:
# Admin area, Settings, Visibility and access controls
default_project_visibility: "public"
default_group_visibility: "public"

# Limit users to 250 projects by default:
default_projects_limit: 250

# List of accepted import sources: Admin Area, Settings, General, Visibility
# and access controls, Import sources:
import_sources:
  - github
  - bitbucket
  - bitbucket_server
  - fogbugz
  - git
  - gitlab_project
  - gitea
  - manifest

# Increase default artifact size limit of 100M, decrease artifact expiry time.
# See https://phabricator.wikimedia.org/T292372 for background.  This is not a
# well-considered strong policy statement or anything, and should definitely be
# revisited:
max_artifacts_size: 350
default_artifacts_expire_in: "1 day"

# Require admin users to authenticate to use admin functions.
# https://gitlab.wikimedia.org/help/user/admin_area/settings/sign_in_restrictions#admin-mode
# https://phabricator.wikimedia.org/T316419
admin_mode: true

# Terms of service - Markdown content:
terms: "Use of the Wikimedia GitLab instance is governed by [the Code of Conduct](https://www.mediawiki.org/wiki/Code_of_Conduct) for Wikimedia technical spaces."

# Indicates whether users can create top-level groups. Introduced in GitLab 15.5. Defaults to true.
# moved from puppet here because of deprecation - T338460
can_create_group: false

# Require 2fa for admins - T361277:
require_admin_two_factor_authentication: true

# Don't force tokens to expire - T385930:
require_personal_access_token_expiry: false
service_access_tokens_expiration_enforced: false
