gitlab-settings
===============

A collection of GitLab CE settings for the [Wikimedia GitLab instance][wmf-gl],
along with Python utilities for applying them via [the settings
API][gl-settings].

[gl-settings]: https://docs.gitlab.com/ce/api/settings.html
[wmf-gl]: https://gitlab.wikimedia.org/

Installing
----------

```sh
pip3 install -r requirements.txt
```

Usage
-----

## Settings

View current instance settings:

```sh
./settings view
```

Show differences between instance settings and what's specified explicitly in
the local `gitlab1004.yaml` settings file:

```sh
./settings diff
```

Update instance settings from `gitlab1004.yaml`:

```sh
./settings update
```

By default GitLab production instance `gitlab.wikimedia.org` is used. If you want to query other instances, make sure to add `--instance` flag. Also add use the correct `--settings-file` matching the host.
For example to update replica instance from `gitlab2002.yaml`:

```sh
./settings --instance https://gitlab-replica-a.wikimedia.org --settings-file ./gitlab1004.yaml update
```

## Appearance

View current instance appearance settings:

```sh
./appearance view
```

Show differences between instance appearance and what's specified explicitly in
the local `appearance.yaml` settings file:

```sh
./appearance diff
```

Update instance appearance from `appearance.yaml`:

```sh
./appearance update
```

By default GitLab production instance `gitlab.wikimedia.org` is used. If you want to query other instances, make sure to add `--instance` flag. The appearance file can be changed by setting `--appearance-file`

```sh
./appearance --instance https://gitlab-replica-a.wikimedia.org --appearance-file ./appearance-replica.yaml update
```

## Projects

Apply project settings (at the moment, this disables wikis and issue tracking
for any projects that have turned it on):

```sh
./configure-projects
```

## Runners
Generate authentication tokens for all runners based on the config in `runner-config.json`:

```sh
./runners generate-tokens
```

The command outputs the tokens to stdout. So make sure to copy the tokens to private puppet/terraform variables.

List active runners, pause them, and unpause them after maintenance:

```sh
./runners active | tee active.txt
./runners pause < active.txt
./runners unpause < active.txt
```
