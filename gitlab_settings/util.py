import difflib
import getpass
import json
import os
import requests
import sys

def get_token():
    if os.environ.get('GITLAB_TOKEN'):
        return os.environ.get('GITLAB_TOKEN')
    return getpass.getpass('GitLab token with api scope: ').strip()

def confirm(question="Continue?", default=False):
    """
    Ask for confirmation from the user if possible, otherwise return default
    when stdin is not attached to a terminal.

    The confirmation is fullfilled when the user types an affirming response
    which can be either 'y' or 'yes', otherwise the default choice is assumed

    The confirmation is rejected when default=False and the user types anything
    other than affirmative.

    :param question: prompt text to show to the user
    :param default: boolean default choice, True [Y/n] or False [y/N]. This is
                    the value that is returned when a tty is not attached to
                    stdin or the user presses enter without typing a response.

    """
    yes = ["y", "yes"]
    no = ["n", "no"]

    if default:
        choices = "[Y/n]"
    else:
        choices = "[y/N]"

    # in case stdin is not a tty or the user accepts the default answer, then
    # the result will be default.
    result = default

    if sys.stdout.isatty():
        ans = input("{} {}: ".format(question, choices)).strip().lower()
        if ans in yes:
            result = True
        elif ans in no:
            result = False

    return result

def diffsettings(old, new):
    output = ''
    d = difflib.Differ()
    old_lines = json.dumps(old, indent=4).splitlines()
    new_lines = json.dumps(new, indent=4).splitlines()
    for thing in d.compare(old_lines, new_lines):
        if thing.startswith('-') or thing.startswith('+') or thing.startswith('?'):
            output += thing

    return output

def get_instance_settings(url, headers):
    try:
        req = requests.get(url, headers=headers)
        req.raise_for_status()
    except:
        raise SystemExit(req.text)
    original_instance_settings = req.json()

    return original_instance_settings

def update_settings(url, headers , local_settings):
    # Do some type juggling so we pass strings instead of booleans to the API:
    for x, y in local_settings.items():
        if y is True:
            local_settings[x] = 'true'
        elif y is False:
            local_settings[x] = 'false'

    try:
        req = requests.put(url, headers=headers, json=local_settings)
        req.raise_for_status()
        new_instance_settings = req.json()
    except:
        raise SystemExit(req.text)

    return new_instance_settings