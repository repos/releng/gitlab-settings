import ldap
import json
import os
import re
import requests
import urllib.parse


class GitlabHelper():
    ACCESS_LEVELS = {
        "No access": 0,
        "Minimal": 5,
        "Guest": 10,
        "Reporter": 20,
        "Developer": 30,
        "Maintainer": 40,
        "Owner": 50,
    }

    def __init__(self, gitlab_url, username, token, logger):
        self.gitlab_api_url = os.path.join(gitlab_url, "api/v4")
        self.gitlab_username = username
        self.gitlab_token = token
        self.logger = logger

        self.session = requests.Session()
        self.session.headers.update({"PRIVATE-TOKEN": self.gitlab_token})

        # Verify that the account associated with the token matches
        # the supplied username.
        user = self.get_current_user()
        if user["username"] != username:
            raise Exception(f"The supplied token was expected to be for the {username} account but it is really for the {user['username']} account.")

    def _post(self, path, *args, **kwargs):
        return self.session.post(os.path.join(self.gitlab_api_url, path),
                                 *args,
                                 **kwargs)

    def _delete(self, path, *args, **kwargs):
        return self.session.delete(os.path.join(self.gitlab_api_url, path),
                                   *args,
                                   **kwargs)

    def _put(self, path, *args, **kwargs):
        return self.session.put(os.path.join(self.gitlab_api_url, path),
                                *args,
                                **kwargs)

    def _get(self, path, absolute=False, *args, **kwargs):
        if absolute:
            url = path
        else:
            url = os.path.join(self.gitlab_api_url, path)

        return self.session.get(url,
                                *args,
                                **kwargs)

    def _get_paginated(self, path, *args, **kwargs):
        """
        Make a GET request to 'path', using *args and **kwargs in
        the call to requests.get().

        The response is expected to be a JSON-encoded list of objects.
        This generator yields each object individually, making additional
        requests for additional pages as needed.
        """
        resp = self._get(path, *args, **kwargs)
        
        while True:
            resp.raise_for_status()

            for item in resp.json():
                yield item

            if not resp.links.get("next"):
                break

            resp = self._get(resp.links["next"]["url"], absolute=True)
        
    def _collect_users(self, path, filter=None) -> dict:
        """
        Returns a dictionary of gitlab users.  The key is the username and the
        value is a dictionary in the format described at
        <https://docs.gitlab.com/ee/api/users.html#for-administrators>.

        If 'filter' is supplied it will be called with each user record.  If the filter
        returns true, the user will be added to the result.
        """

        res = {}
        for user in self._get_paginated(path):
            if filter is None or filter(user):            
                res[user["username"]] = user

        return res

    def _quoted_path(self, path):
        return urllib.parse.quote(path, safe='')

    ###########

    def get_current_user(self) -> dict:
        """
        Returns a dictionary in the format described at
        <https://docs.gitlab.com/ee/api/users.html#for-administrators>.
        """
        resp = self._get("user")
        resp.raise_for_status()
        return resp.json()

    def get_all_users(self) -> dict:
        """
        Returns a dictionary of gitlab users.  The key is the username and the
        value is a dictionary in the format described at
        <https://docs.gitlab.com/ee/api/users.html#for-administrators>.
        """
        return self._collect_users("users")

    def get_user(self, username) -> dict:
        """
        If the user is found, returns a dictionary in the format described at
        <https://docs.gitlab.com/ee/api/users.html#for-administrators>.  Otherwise
        returns None.
        """
        quoted_username = self._quoted_path(username)

        res = self._collect_users(f"users?username={quoted_username}",
                                  filter=lambda user: user['username'] == username)

        if len(res) == 0:
            return None

        if len(res) == 1:
            for user in res.values():
                return user

        raise Exception(f"Found more than one user matching {username}: {res}")

    def get_user_by_email(self, email) -> dict:
        quoted_email = self._quoted_path(email)

        res = self._collect_users(f"users?search={quoted_email}")

        if len(res) == 0:
            return None

        if len(res) == 1:
            for user in res.values():
                return user

        raise Exception(f"Found more than one user matching {email}: {res}")

    def get_group_members(self, group_name, all=False) -> dict:
        """ Returns a dictionary of gitlab users.  The key is the
        username and the value is a dictionary in the format described
        at
        <https://docs.gitlab.com/ee/api/users.html#for-administrators>.
        """
        quoted_group_name = self._quoted_path(group_name)

        url = f"groups/{quoted_group_name}/members"
        if all:
            url = os.path.join(url, "all")

        if self.logger:
            self.logger.info(f"Collecting member list of Gitlab group {group_name}")

        return self._collect_users(url)

    def get_group_members_added_by_user_id(self, group_name, user_id):
        """ Returns a dictionary of gitlab users.  The key is the
        username and the value is a dictionary in the format described
        at
        <https://docs.gitlab.com/ee/api/users.html#for-administrators>.
        """
        quoted_group_name = self._quoted_path(group_name)

        if self.logger:
            self.logger.info(f"Collecting member list of Gitlab group {group_name}")

        def filter(user):
            # Though the field is called created_by, it stores the time
            # when the user was added as a member to the group.
            created_by = user.get("created_by")
            
            if not created_by:
                return False

            return created_by["id"] == user_id

        return self._collect_users(f"groups/{quoted_group_name}/members", filter=filter)

    def create_user(self, params: dict) -> dict:
        if self.logger:
            self.logger.info(f"Creating gitlab user {params['username']}")

        resp = self._post("users", params=params)
        resp.raise_for_status()
        return resp.json()

    def _add_ssh_key(self, user_id, key, title):
        resp = self._post(f"users/{user_id}/keys", json={
            "id": user_id,
            "key": key,
            "title": title,
        })

        if resp.status_code == 400:
            if self.logger:
                fail = resp.json()
                self.logger.warning(f"Got 400 response when creating ssh key: {key}.  Response json is:\n{fail}")
            return

        resp.raise_for_status()

    def create_user_from_ldap(self, ldap_helper, username) -> dict:
        """
        Use information from an LDAP server to create a GitLab user, including importing
        registered ssh keys.

        If creation is successful, returns a dictionary in the format
        described at <https://docs.gitlab.com/ee/api/users.html#for-administrators>.

        Returns None if user creation fails due to a conflict with another account (for
        example, if the email address is already used), or if a corresponding ldap user 
        was not found.
        """
        info = ldap_helper.get_user_info(username)
        if info is None:
            return None

        display_name = info["display_name"]
        email = info["email"]

        if not email:
            self.logger.error(f"Failed to create user {username} with no email address.\nSkipping.")
            return None

        params = {
            "username": username,
            "name": display_name,
            "email": email,
            "force_random_password": True,
            "skip_confirmation": True,
            "provider": "openid_connect",
            "extern_uid": username,
        }

        try:
            new_user = self.create_user(params)
        except requests.exceptions.HTTPError as e:
            resp = e.response

            if resp.status_code == requests.codes['conflict']:  # 409
                message = resp.json()["message"]

                self.logger.error(f"Failed to create user {username} with email address {email}: {message}\nSkipping.")

                conflicting_user = self.get_user_by_email(email)
                if conflicting_user:
                    self.logger.error(f"The conflicting address is held by {conflicting_user['username']}: {conflicting_user['web_url']}")

                return None
            # Something unexpected
            raise

        id = new_user['id']

        def get_key_title(key):
            res = key.split(maxsplit=2)
            if len(res) >= 3:
                return res[2]
            else:
                return "ssh key"

        for key in info["ssh_public_keys"]:
            self._add_ssh_key(id, key, get_key_title(key))

        return new_user

    def add_identity(self, user_id, provider, extern_uid) -> dict:
        """
        Returns the updated user data.
        """
        params = {
            "provider": provider,
            "extern_uid": extern_uid,
        }
        try:
            resp = self._put(f"users/{user_id}", params=params)
            resp.raise_for_status()
        except requests.exceptions.HTTPError as e:
            # In hopes of debugging transient HTTP 400 responses.
            print(f"Failed to add identity {params} to user id {user_id}: {e}")
            print(e.response.json())
            raise
        return resp.json()

    def _translate_access_level(self, access_level):
        code = self.ACCESS_LEVELS.get(access_level.title())
        if code is None:
            raise Exception(f"Unknown access level: {access_level}")
        return code

    def add_user_to_group(self, user, group_name, access_level):
        """
        'user' must be a dictionary representing a gitlab user.
        """
        quoted_group_name = self._quoted_path(group_name)

        if self.logger:
            self.logger.info(f"Adding {user['username']} to group {group_name}")

        params = {
            # "id": group_id,
            "user_id": user["id"],
            "access_level": self._translate_access_level(access_level),
        }

        resp = self._post(f"groups/{quoted_group_name}/invitations", params=params)
        resp.raise_for_status()

    def remove_user_from_group(self, user, group_name):
        """
        'user' must be a dictionary representing a gitlab user.
        """
        quoted_group_name = self._quoted_path(group_name)

        if self.logger:
            self.logger.info(f"Removing {user['username']} from group {group_name}")

        user_id = user["id"]
        resp = self._delete(f"groups/{quoted_group_name}/members/{user_id}")
        resp.raise_for_status()

    def update_group(self, group_name, ldap_helper=None, to_create=None, to_add=None, to_remove=None, confirm=True,
                     access_level="Developer"):
        """
        ldap_helper must be supplied if to_create is supplied.
        """

        if to_create is None:
            to_create = []
        else:
            if not ldap_helper:
                raise ValueError("ldap_helper must be supplied if to_create is supplied")
            self.logger.info(f"There are {len(to_create)} GitLab users to create.")

        if to_add is None:
            to_add = []
        else:
            self.logger.info(f"There are {len(to_add)} members to add to {group_name}.")

        if to_remove is None:
            to_remove = []
        else:
            self.logger.info(f"There are {len(to_remove)} members to remove from {group_name}.")

        if not to_create and not to_add and not to_remove:
            self.logger.info("No work to do.")
            return

        if confirm:
            print("Proceed? ", end='')
            if input() != 'y':
                # FIXME: Maybe just return None or something
                raise SystemExit("Cancelled")

        creation_failures = 0

        for user in to_add:
            gitlab_user = self.get_user(user)
            if not gitlab_user:
                gitlab_user = self.create_user_from_ldap(ldap_helper, user)
                if not gitlab_user:
                    creation_failures += 1
                    continue

            self.add_user_to_group(gitlab_user, group_name, access_level)

        for user in to_remove:
            self.remove_user_from_group(user, group_name)

        if creation_failures:
            self.logger.info(f"Failed to create {creation_failures} users.\n")


class LdapHelper():
    def __init__(self, url):
        self.conn = ldap.initialize(url)

    def get_group_members(self, group) -> list:
        """Returns a list of members of the ldap group"""

        r = self.conn.search_s("ou=groups,dc=wikimedia,dc=org", ldap.SCOPE_ONELEVEL, f"cn={group}", ["member"])
        if len(r) == 0:
            raise SystemExit(f"Did not find an LDAP group named {group}")
        assert len(r) == 1
        (dn, attrs) = r[0]
        assert dn == f'cn={group},ou=groups,dc=wikimedia,dc=org'

        return [re.match(r"uid=(.*),ou=people,dc=wikimedia,dc=org$", entry.decode("UTF-8"))[1] for entry in attrs["member"]]

    def get_user_info(self, username) -> dict:
        """
        Returns None if the LDAP user is not found.
        """
        r = self.conn.search_s("ou=people,dc=wikimedia,dc=org", ldap.SCOPE_SUBTREE, f"uid={username}")
        hits = len(r)
        if hits == 0:
            return None
        if hits != 1:
            raise Exception(f"get_user_info: LDAP server returned {hits} result(s) for user '{username}'. Expected just one.")
        
        (dn, attrs) = r[0]

        email = self._get_singleton_attribute(attrs, "mail")
        display_name = self._get_singleton_attribute(attrs, "cn")
        try:
            ssh_public_keys = self._get_attribute(attrs, "sshPublicKey")
        except KeyError:
            ssh_public_keys = []

        return {
            "email": email,
            "display_name": display_name,
            "ssh_public_keys": ssh_public_keys,
        }

    def _get_attribute(self, attrs, name):
        return list(map(bytes.decode, attrs[name]))

    def _get_singleton_attribute(self, attrs, name):
        res = self._get_attribute(attrs, name)
        if len(res) != 1:
            raise ValueError(f"Expected 1 value for attribute {name} but got {res}")
        return res[0]


class GerritHelper():
    def __init__(self, url, username, password):
        self.gerrit_url = url
        self.gerrit_username = username
        self.gerrit_password = password

    def get_group_id(self, group_name):
        resp = requests.get(os.path.join(self.gerrit_url, "a", "groups/"),
                            params={"query": f"name:{group_name}"},
                            auth=(self.gerrit_username, self.gerrit_password))
        resp.raise_for_status()
        (weird, data) = resp.text.splitlines()
        if weird != ")]}'":
            raise Exception(f"Unexpected response from Gerrit:\n{resp.text}")

        data = json.loads(data)

        if len(data) == 0:
            return None

        if len(data) == 1:
            return data[0]["id"]

        raise Exception(f"Unexpected results: {data}")

    def get_group_detail(self, group_id) -> dict:
        resp = requests.get(os.path.join(self.gerrit_url, "a", "groups", group_id, "detail"),
                            auth=(self.gerrit_username, self.gerrit_password))
        resp.raise_for_status()
        (weird, data) = resp.text.splitlines()
        if weird != ")]}'":
            raise Exception(f"Unexpected response from Gerrit:\n{resp.text}")

        return json.loads(data)

    def get_referenced_ldap_groups(self, group_id) -> list:
        """
        Returns a list of unique LDAP group names referenced by the Gerrit group
        identified by group_id.
        """
        res = []

        detail = self.get_group_detail(group_id)

        for include in detail["includes"]:
            m = re.match("ldap/(.*)", include["name"])
            if m:
                res.append(m[1])
            else:
                res += self.get_referenced_ldap_groups(include["id"])

        return list(set(res))

    def get_non_ldap_group_members(self, group_id, res=None) -> dict:
        if res is None:
            res = {}

        detail = self.get_group_detail(group_id)

        for member in detail["members"]:
            username = member["username"]
            if not res.get(username):
                res[username] = member

        for include in detail["includes"]:
            if not include["name"].startswith("ldap/"):
                res = self.get_non_ldap_group_members(include["id"], res)

        return res
