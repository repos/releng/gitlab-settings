Utilities for managing GitLab group membership
==============================================

`sync-gitlab-group-with-ldap`
Synchronize a Gitlab group with one or more Wikimedia LDAP groups. Gitlab user accounts will be created if needed.

`import-gerrit-group`
Reads the list of members of the supplied gerrit group and adds them to the supplied gitlab group, creating users as needed.  Gerrit group members that are members of a referenced LDAP group are ignored on the assumption that these members will be maintained using `sync-gitlab-group-with-ldap`.

`clean-gitlab-group`
Remove members from a Gitlab group which were added by a particular user.

Configuration
-------------

Copy `config.yaml.example` to `config.yaml` and populate the fields with appropriate data.
